#!/usr/bin/python
import lxml.objectify
import lxml.etree
import qualysapi
import getpass

# Setup connection to QualysGuard API.
# Configura la conexion con el API de QualysGuard, ademas haciendo uso del archivo
# .qcrc para establecer la conexion 
qgc = qualysapi.connect()

# Session management
print 'QualysGuard API v2 - Login\n'
username = raw_input('Insert your username: ')
password = getpass.getpass()

# API v2 call - Session Login
call = '/api/2.0/fo/session/'
# Parameters to action: Login
parameters = {'action': 'login', 'username': username, 'password': password}
# Request to the API
xml_output = qgc.request(call, parameters)
# Load the response (xml format)
root = lxml.objectify.fromstring(xml_output)
# Accept or reject the connection
if root.RESPONSE.TEXT == 'Logged in':
    print '\nLogin success!\n'

# API v2 call - Host Assests List
call = '/api/2.0/fo/asset/host/'
parameters = {'action': 'list', 'details': 'All/AGs', 'ag_titles': 'xxx', 'truncation_limit': '10000'}
xml_output = qgc.request(call, parameters)
# Load the response (xml format)
root = lxml.objectify.fromstring(xml_output)
# Iterate hosts and print out DNS name.
pairs = {}
for host in root.RESPONSE.GLOSSARY.ASSET_GROUP_LIST.ASSET_GROUP:
    pairs[host.ID.xpath("string()")] = host.TITLE.xpath("string()")

for ag_name in root.RESPONSE.HOST_LIST.HOST:
    ag_names = ag_name.ASSET_GROUP_IDS.xpath("string()")
    ag_names.strip()
    ag_array = []

    for i in ag_names.split(','):
        ag_array.append(pairs[i])
        tmp = ','.join(ag_array)

    ag_name.ASSET_GROUP_IDS = tmp

# Save the proccesed file to a string
xml_final = lxml.etree.tostring(root, encoding="UTF-8")

# Open or create a file named 'Info.xml'
info = open("Asset Report.xml", "w")
# Write or overwrite the file
info.write(xml_final);
# Close opend file
info.close()

print 'Report finished \n'
# API v2 call - Session Logout
call = '/api/2.0/fo/session/'
parameters = {'action': 'logout'}
# Request to the API
xml_logout = qgc.request(call, parameters)

# Load the response (xml format)
root = lxml.objectify.fromstring(xml_logout)
# Close the connection
if root.RESPONSE.TEXT == 'Logged out':
    print 'Logout success!'

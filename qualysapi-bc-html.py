#!/usr/bin/python
import lxml.objectify
import lxml.etree
import qualysapi
import getpass

# Setup connection to QualysGuard API.
# Configura la conexion con el API de QualysGuard, ademas haciendo uso del archivo
# .qcrc para establecer la conexion 
qgc = qualysapi.connect()

# Session management
print 'QualysGuard API v2 - Login\n'
username = raw_input('Insert your username: ')
password = getpass.getpass()

# API v2 call - Session Login
call = '/api/2.0/fo/session/'
# Parameters to action: Login
parameters = {'action': 'login', 'username': username, 'password': password}
# Request to the API
xml_output = qgc.request(call, parameters)
# Load the response (xml format)
root = lxml.objectify.fromstring(xml_output)
# Accept or reject the connection
if root.RESPONSE.TEXT == 'Logged in':
    print '\nLogin success!\n'

# API v2 call - Host Assests List
call = '/api/2.0/fo/knowledge_base/vuln/'
parameters = {'action': 'list', 'details': 'All', 'ids': 'xxx,yyy,zzz'}
xml_output = qgc.request(call, parameters)

# Possible HTML Tags
dic = {'&lt;P&gt;': '', '</P>': '', '&lt;B&gt;': '', '&lt;/B&gt;': '>\n', 
       '&lt;BR&gt;': '\n', '&lt;A HREF="': ' ', '" TARGET="_blank"&gt;': ' - ',
       '&lt;/A&gt;': ''};

# Load the response (xml format)
root = lxml.objectify.fromstring(xml_output)
# Save the proccesed file to a string
xml_final = lxml.etree.tostring(root, encoding="UTF-8")
for item in dic:
    xml_final = xml_final.replace(item,dic[item])

# Open or create a file named 'Info.xml'
info = open("Document without HTML.xml", "w")
# Write or overwrite the file
info.write(xml_final);
# Close opend file
info.close()



print 'Report finished \n'
# API v2 call - Session Logout
call = '/api/2.0/fo/session/'
parameters = {'action': 'logout'}
# Request to the API
xml_logout = qgc.request(call, parameters)

# Load the response (xml format)
root = lxml.objectify.fromstring(xml_logout)
# Close the connection
if root.RESPONSE.TEXT == 'Logged out':
    print 'Logout success!'
